const express = require('express');

const app = express();

app.use(require('./middleware'));

app.get('/:number', (req, res) => {
  const { params } = req;

  const num = Number(params.number);

  let msg = 'Is not correct number';

  if(num % 3 === 0 && num % 5 === 0) {
    msg = 'Fizz Buzz!!!';
  } else if (num % 5 === 0) {
    msg = 'Buzz!!!';
  } else if (num % 3 === 0) {
    msg = 'Fizz!!!'
  }

  res.json({ msg });
});


app.listen(process.env.PORT, console.dir);